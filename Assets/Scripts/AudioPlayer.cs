﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {

	public static AudioPlayer Instance;
	public AudioClip[] clips;
	AudioSource[] players;

	void Awake(){
		if(Instance == null){
			players = GetComponents<AudioSource>();
			Instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else{
			Destroy(gameObject);
		}
	}

	public static void PlayClip(string clipName){
		AudioClip clipToPlay = new AudioClip();
		foreach(var clip in Instance.clips){
			if(clip.name == clipName){
				clipToPlay = clip;
				break;
			}
		}
		foreach(var player in Instance.players){
			if(!player.isPlaying){
				player.PlayOneShot(clipToPlay);
				break;
			}
		}
	}

	public static void PlayLoop(string clipName){
		AudioClip clipToPlay = new AudioClip();
		foreach(var clip in Instance.clips){
			if(clip.name == clipName){
				clipToPlay = clip;
				break;
			}
		}
		foreach(var player in Instance.players){
			if(!player.isPlaying){
				player.clip = clipToPlay;
				player.loop = true;
				player.Play();
				break;
			}
		}
	}

	public static void StopCLip(string clipName){
		foreach(var player in Instance.players){
			if(player.clip.name == clipName){
				player.Stop();
				break;
			}
		}
	}

}

