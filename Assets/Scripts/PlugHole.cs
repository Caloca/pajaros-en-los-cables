﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PlugHole : MonoBehaviour, IDropHandler
{
    public int index;
    public bool tutorialLlamada;

    public SpriteRenderer led;

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("<color=orange>"+this+" drop "+ eventData.pointerDrag.name + "</color>");
        eventData.pointerDrag.GetComponent<PlugHandle>().isOnHole = true;
        eventData.pointerDrag.GetComponent<PlugHandle>().holeIndex = index;

        eventData.pointerDrag.transform.position = this.transform.position;
        eventData.pointerDrag.GetComponent<PlugHandle>().CheckContraints();

        CheckForTutorial();
    }

    public void SetLedState(ledState state){
        switch(state){
            case ledState.available:
                led.color = Color.gray;
                StopCoroutine("ParpadeoLed");
                break;
            case ledState.incoming:
                led.color = Color.yellow;
                StartCoroutine("ParpadeoLed");
                break;
            case ledState.busy:
                led.color = Color.green;
                StopCoroutine("ParpadeoLed");
                break;
            case ledState.failed:
                led.color = Color.red;
                StopCoroutine("ParpadeoLed");
                break;
        }
    }

    private void CheckForTutorial()
    {
        if(PlugsManager.Instance.isTutorial && tutorialLlamada)
        {
            PlugsManager.Instance.historia.ExecuteBlock("Tutorial2");
            PlugsManager.Instance.EndTutorial();
        }
    }

    IEnumerator ParpadeoLed(){
        AudioPlayer.PlayLoop("ring2");
        while(true){
            if(Time.time-(int)Time.time<0.2f ){
                if(led.color==Color.yellow)
                    led.color = Color.grey;
            }
            else if(led.color == Color.grey){
                led.color = Color.yellow;
            }
            yield return 0;
        }
    }
    
}

public enum ledState{
    available,
    incoming,
    busy,
    failed
}

