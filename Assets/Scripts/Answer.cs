﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Answer : ScriptableObject 
{
    public float timeToSolve = 60f;
    public Vector2Int connection;
}
