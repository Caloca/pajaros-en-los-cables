﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plug : MonoBehaviour 
{
    public PlugHandle handlerA;
    public PlugHandle handlerB;

    public void CheckForConnections()
    {
        if(handlerA.isOnHole && handlerB.isOnHole)
            PlugsManager.Instance.CheckAnswer(this);
    }

    public void StartCall(){
        
    }

    public void EndCall(){
        
    }
}
