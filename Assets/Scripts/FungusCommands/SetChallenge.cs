﻿using Fungus;
using UnityEngine;

[CommandInfo("Custom", 
             "Set Challenge", 
             "Sets the current puzzle")]
[AddComponentMenu("")]
public class SetChallenge : Command 
{
    [SerializeField] public Answer challengeToSet;

    public override void OnEnter()
    {
        PlugsManager.Instance.currentAnswer = challengeToSet;
        Continue();
    }

    public override Color GetButtonColor()
    {
        return new Color32(175, 240, 25, 255);
    }
}
