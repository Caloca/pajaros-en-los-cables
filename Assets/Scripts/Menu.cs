﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public GameObject menu, creditos;
	public Image fadeImage;

	public void Comenzar () {
		StartCoroutine(FadeToScene());
	}
	
	public void Creditos () {
		menu.SetActive(false);
		creditos.SetActive(true);
	}
	public void Regresar(){
		menu.SetActive(true);
		creditos.SetActive(false);
	}

	IEnumerator FadeToScene(){
		fadeImage.gameObject.SetActive(true);
		var time = 0f;
		while(time<1.5f){
			time += Time.deltaTime;
			fadeImage.color = Color.Lerp(Color.clear,Color.black,time/1.5f);
			yield return null;
		}
		fadeImage.color = Color.black;
		SceneManager.LoadScene("Main 1");
	}
}