﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlugHandle : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    //Varibales
    //--------------------------------------------
    public Sprite[] plugState;//0-desconectado, 1-conectado
    public GameObject unpluggedPlug;
    public Transform handle;

    [HideInInspector]
    public bool isOnHole;
    [HideInInspector]
    public int holeIndex;

    private Rigidbody2D rBody;
    private Collider2D collider;
    private SpriteRenderer spriteRenderer;
    private Collider2D unpluggedCollider;

    //Unity Methods
    //--------------------------------------------
    void Start()
    {
        rBody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        unpluggedCollider = unpluggedPlug.GetComponent<Collider2D>();

        spriteRenderer.enabled = false;
        unpluggedPlug.SetActive(true);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        spriteRenderer.enabled = false;
        isOnHole = false;
        collider.enabled = false;
        rBody.constraints = RigidbodyConstraints2D.FreezePosition;
        unpluggedCollider.enabled = false;
        unpluggedPlug.SetActive(true);
        AudioPlayer.PlayClip("unplug");
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = Camera.main.ScreenToWorldPoint(eventData.position).setZ(0);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        collider.enabled = true;
        CheckContraints();
        transform.parent.GetComponent<Plug>().CheckForConnections();
    }

    //Private Methods
    //--------------------------------------------
    public void CheckContraints()
    {
        if(!isOnHole)
        {
            rBody.constraints = RigidbodyConstraints2D.None;
            unpluggedCollider.enabled = true;
        }
        else
        {
            rBody.constraints = RigidbodyConstraints2D.FreezePosition;
            spriteRenderer.enabled = true;
            unpluggedPlug.SetActive(false);
            AudioPlayer.PlayClip("estatica plug");
        }
    }
}
