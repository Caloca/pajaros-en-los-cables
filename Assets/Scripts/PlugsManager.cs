﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class PlugsManager : MonoBehaviour 
{
    public static PlugsManager Instance;

    public bool isTutorial;
    public List<PlugHole> allPlugs = new List<PlugHole>();
    public Answer currentAnswer{
        get{
            return _currentAnswer;
        }
        set{
            _currentAnswer = value;
            foreach(var pinshiplug in allPlugs) pinshiplug.SetLedState(ledState.available);
            allPlugs[_currentAnswer.connection.x].SetLedState(ledState.incoming);
        }
    }
    private Answer _currentAnswer;

    public Flowchart historia;
    public Flowchart requests;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        for(int i=0; i<allPlugs.Count; i++)
        {
            allPlugs[i].index = i;
        }
    }

    public void CheckAnswer(Plug plugToCheck)
    {
        Vector2Int sentAnswer = new Vector2Int(plugToCheck.handlerA.holeIndex, plugToCheck.handlerB.holeIndex);
        Vector2Int reverseAnswer = new Vector2Int(plugToCheck.handlerB.holeIndex, plugToCheck.handlerA.holeIndex);

        
        if((sentAnswer.x == 15 || reverseAnswer.x == 15)  && (currentAnswer.connection.x == sentAnswer.x || currentAnswer.connection.x == reverseAnswer.x))
        {
            requests.ExecuteBlock(bloque);
            AudioPlayer.StopCLip("ring2");
        }
        else if(currentAnswer.connection == sentAnswer || currentAnswer.connection == reverseAnswer)
        {
            Debug.Log("This is correct bruh");
            AudioPlayer.StopCLip("ring2");
            allPlugs[plugToCheck.handlerA.holeIndex].SetLedState(ledState.busy);
            allPlugs[plugToCheck.handlerB.holeIndex].SetLedState(ledState.busy);
            StartCoroutine(WaitForCall(currentAnswer.timeToSolve,plugToCheck));

            historia.ExecuteBlock(bloque);
            contadorHistoria++;
        }
        else
        {
            Debug.Log("keep tryin m8");
        }
    }

    public void EndTutorial()
    {
        isTutorial = false;
    }

    IEnumerator WaitForCall(float seconds,Plug plugToCheck){
        plugToCheck.StartCall();
        yield return new WaitForSeconds(seconds);
        plugToCheck.EndCall();
        allPlugs[plugToCheck.handlerA.holeIndex].SetLedState(ledState.available);
        allPlugs[plugToCheck.handlerB.holeIndex].SetLedState(ledState.available);
    }

    public int contadorHistoria;
    

    string bloque{
        get{
            switch(contadorHistoria){
                case 0: return "Tutorial3";
                case 1: return "Llamada1";
                case 2: return "Llamada2";
                case 3: return "Llamada3";
                case 4: return "Llamada4";
                case 5: return "Llamada5";
                case 6: return "Llamada6";
                case 7: return "Llamada7";
                default: return null;
            }
        }
    }
    public bool enableBlockOverride;
    void OnValidate(){
        if(!enableBlockOverride) return;
        requests.StopAllBlocks();
        requests.ExecuteBlock(bloque);
    }
}